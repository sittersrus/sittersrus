<?php
use search_main;

class EmptyTest extends TestCase
{
    public function testSitterSearchEmpty()
    {
        $keyword = "";
        $this->assertEmpty($keyword);

        return $keyword;
    }

    
    public function testKeywordSitterSearch($keyword)
    {
        returnSitterResults($keyword)
        $this->assertEquals($result);
        $this->assertNotEmpty($result);

        return $keyword;
    }

    
    public function testNoSitterResult($keyword)
    {
    	returnListingResults("there will never be a result for this");
        $this->assertEquals("O results");
        $this->assertEmpty($result);
    }
    
     public function testListingSearchEmpty()
    {
        $keyword = "";
        $this->assertEmpty($keyword);

        return $keyword;
    }
    
    public function testKeywordListingSearch($keyword)
    {
        returnSitterResults($keyword)
        $this->assertEquals($result);
        $this->assertNotEmpty($result);

        return $keyword;
    }

  
    public function testNoSitterResult($keyword)
    {
    	returnSitterResults("there will never be a result for this");
        $this->assertEquals("O results");
        $this->assertEmpty($result);
    }
    
}
?>